<!DOCTYPE html>
<html>
	<head>
		<title>Homepage-Maken.nl - Hét startpunt voor webmasters</title>
		<meta name="description" content="Gratis je eigen homepage of website maken? Leer het hier!">
		<?php include('resources/includes/inside_head.php'); ?>
	</head>
	<body>
		<?php include('resources/includes/side_nav.php'); ?>
		<?php include('resources/includes/side_nav_r.php') ?>
		<div class="main_div">
			<h1>
				Homepage-Maken.nl - Hét startpunt voor webmasters
			</h1>
			<p>
				Hier vind je alles over het maken van je eigen website.</br>
				<b>Links</b> vind je alles over webdesign, onder andere tutorials over <abbr title="HyperText Markup Language">HTML</abbr>, <abbr title="Cascading Style Sheet">CSS</abbr>, etc. en verschillende artikelen.
				<b>Rechts</b>	kun je allerlei gratis <i>webtools</i> vinden, zoals tellers, gastenboeken en fora.
			</p>
			<h3>
				Waar te beginnen?
			</h3>
			<p>
				Klik op een link in de linkerzijnavigatie als je een ervaren webdesigner bent.</br>
				Voor beginners is er een <a href="beginner.php">speciale pagina</a>.
			</p>
			<p>
				Bekijk de verschillende boeken op onze <a href="boeken.php">boekenafdeling</a>.
			</p>
			<hr/>
			<?php include('resources/includes/legal.php') ?>
		</div>
	</body>
</html>