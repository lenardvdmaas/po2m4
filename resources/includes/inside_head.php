<link rel="shortcut icon" href="../images/favicon.png"/>

<link rel="stylesheet" type="text/css" href="../main_style.css"/>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

<noscript>JavaScript wordt niet ondersteund of is uitgeschakeld in deze browser.</noscript>

<meta charset="utf-8"/>
<meta name="viewport" content="width=device-width, initial-scale=1.0">

<!-- Plaats elk CSS-bestand BOVEN het commentaar hieronder (het liefst onder de tags met rel="stylesheet") -->
<!--[if lt IE 9]>
		<script src="../html5shiv/dist/html5shiv.js"></script>
<![endif]-->